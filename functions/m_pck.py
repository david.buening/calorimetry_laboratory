import os
import sys
import time
import json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# This if statement enables you to write and run programs that test functions directly at the end of this file.
if __name__ == "__main__":
    import pathlib

    file_path = os.path.abspath(__file__)
    file_path = pathlib.Path(file_path)
    root = file_path.parent.parent
    sys.path.append(str(root))

from functions import m_json 


def check_sensors() -> None:
    """Retrieves and prints the serial number and current temperature of all DS18B20 Temperature Sensors.

    This function utilizes the `w1thermsensor` library to interface with the DS18B20 temperature sensors.
    For each available sensor, it prints its serial number (or ID) and the current temperature reading.

    Examples:
    Assuming two DS18B20 sensors are connected:
    >>> check_sensors()
    Sensor 000005888445 has temperature 25.12
    Sensor 000005888446 has temperature 24.89

    """
    # TODO: Print serials and temperature values of all connected sensors.
    
    from w1thermsensor import W1ThermSensor
    
    #Alle erreichbaren Sensoren auslesen und Sensor ID und Temperatur ausgeben
    for sensor in W1ThermSensor.get_available_sensors():
        print("Sensor %s has temperature %.2f" % (sensor.id, sensor.get_temperature()))

    # DONE #


def get_meas_data_calorimetry(metadata: dict) -> dict:
    """Collects and returns temperature measurement data from DS18B20 sensors based on the provided metadata.

    This function initializes sensor objects based on the metadata, prompts the user to start the
    measurement, and then continually reads temperature values until interrupted (e.g., via Ctrl-C).
    It logs the temperatures and the corresponding timestamps. Refer to README.md section
    "Runtime measurement data" for a detailed description of the output data structure.

    Args:
        metadata (dict): Contains details like sensor uuid, serials, and names.
                         Refer to README.md section "Runtime metadata" for a detailed description
                         of the input data structure.

    Returns:
        dict: A dictionary with sensor uuid as keys, and corresponding lists of temperatures and timestamps.

    Example:
        Input metadata:
        {
            "sensor": {
                "values": ["sensor_1", "sensor_2"],
                "serials": ["000005888445", "000005888446"],
                "names": ["FrontSensor", "BackSensor"]
            }
        }

        Output (example data after interruption):
        {
            "sensor_1": [[25.12, 25.15], [0, 2]],
            "sensor_2": [[24.89, 24.92], [0, 2]]
        }

    """
    # Initialize an empty dictionary for storing temperature measurements.
    # The structure is uuid: [[temperatures], [timestamps]].
    data = {i: [[], []] for i in metadata["sensor"]["values"]}
    start = time.time()
    sensor_list = [
        W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
    ]

    input("Press any key to start measurement... <Ctrl+C> to stop measurement")
    try:
        while True:
            for i, sensor in enumerate(sensor_list):
                # TODO: Get experimental data.
                #Temperaturen messen
                temperature = sensor.get_temperature()
                print(temperature)
                #Zeitschritte festhalten mit Differenz aus Startzeit und jetzt
                timestamp = time.time() - start
                #uuid aus metadata auslesen
                sensor_id = metadata["sensor"]["values"][i]
                #Temperatur und Zeit anhängen
                data[sensor_id][0].append(temperature)
                data[sensor_id][1].append(timestamp)    
             
                #AUFRUF: get_meas_data_calorimetry(add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')))
                # DONE #
            # Print an empty line for better readability in the console.
        print("")
    # Catch the KeyboardInterrupt (e.g., from Ctrl-C) to stop the measurement loop.
    except KeyboardInterrupt:
        # Print the collected data in a formatted JSON structure.
        print(json.dumps(data, indent=4))
    # Always execute the following block.
    finally:
        # Ensure that the lengths of temperature and timestamp lists are the same for each sensor.
        for i in data:
            # If the temperature list is longer, truncate it to match the length of the timestamp list.
            if len(data[i][0]) > len(data[i][1]):
                data[i][0] = data[i][0][0 : len(data[i][1])]
            # If the timestamp list is longer, truncate it to match the length of the temperature list.
            elif len(data[i][0]) < len(data[i][1]):
                data[i][1] = data[i][1][0 : len(data[i][0])]

    return data


def logging_calorimetry(
    data: dict,
    metadata: dict,
    data_folder: str,
    json_folder: str,
) -> None:
    """Logs the calorimetry measurement data into an H5 file.

    This function creates a folder (if not already present) and an H5 file with a
    specific structure. The data from the provided dictionaries are written to the
    H5 file, along with several attributes.

    Args:
        data (dict): Contains sensor data including temperature and timestamp.
                     Refer to README.md section"Runtime measurement data" for a detailed
                     description of the data structure.
        metadata (dict): Contains metadata. Refer to README.md section "Runtime metadata"
                         for a detailed description of the structure.
        data_folder (str): Path to the folder where the H5 file should be created.
        json_folder (str): Path to the folder containing the datasheets.

    """
    # Extract the folder name from the provided path to be used as the H5 file name.
    log_name = data_folder.split("/")[-1]
    # Generate the full path for the H5 file.
    dataset_path = "{}/{}.h5".format(data_folder, log_name)
    # Check and create the logging folder if it doesn't exist.
    if not os.path.exists(data_folder):
        os.makedirs(data_folder)

    # Create a new H5 file.
    f = h5py.File(dataset_path, "w")
    # Create a 'RawData' group inside the H5 file.
    grp_raw = f.create_group("RawData")

    # TODO: Add attribute to HDF5.
    
    #AUFRUF1: logging_calorimetry(get_meas_data_calorimetry(m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json'))),add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')),'/home/pi/calorimetry_home/data','/home/pi/calorimetry_home/datasheets')
    #aktuelle Zeit abspeichern
    from datetime import datetime
    now = datetime.now()

    #group_info als gesuchten type festlegen
    type_to_find = 'group_info'
    group_uuid = metadata.get(type_to_find, {}).get('values', [])

    #uuid in einen string konvertieren
    group_uuid_string = ', '.join(group_uuid)
       
    #Werte der jeweiligen keys aus der group.json datei auslesen
   
    # 'created' mit Datum und Zeit versehen und die anderen Attribute aus group.json auslesen
    f.attrs["created"] = now.strftime("%d-%m-%Y %H:%M:%S")
    f.attrs["experiment"] = m_json.get_json_entry(json_folder, group_uuid_string, ['group', 'experiment'])
    f.attrs["group_number"] = m_json.get_json_entry(json_folder, group_uuid_string, ['group', 'number'])
    f.attrs["authors"] = m_json.get_json_entry(json_folder, group_uuid_string, ['group', 'author'])
  
    # DONE #

    # TODO: Write data to HDF5.
    # uuids der Sensoren aus Data auslesen
    for sensor_uuids in data:
        #Gruppe pro uuid erstellen
        grp_sensor = grp_raw.create_group(sensor_uuids)
        #Sensor Name und Serialnummer aus metadata auslesen
        index = metadata['sensor']['values'].index(sensor_uuids)
        sensor_name = metadata['sensor']['names'][index]
        sensor_serial = metadata['sensor']['serials'][index]
        #Name und Serial als Attribut für Sensor Gruppe festlegen
        grp_sensor.attrs["name"] = sensor_name
        grp_sensor.attrs["serial"] = sensor_serial
        #Erstellen der Datasets für Temperaturen und Zeiten in den jeweiligen Sensor Gruppen
        temperatures_ds = grp_sensor.create_dataset("temperatures", data = data[sensor_uuids][0])
        timestamps_ds = grp_sensor.create_dataset("timestamps", data = data[sensor_uuids][1])
        #Einheiten als Attribute der Datasets hinzufügen
        temperatures_ds.attrs["unit"] = "degree celsius"
        timestamps_ds.attrs["unit"] = "seconds"
 
    # DONE #

    # Close the H5 file.
    f.close()


if __name__ == "__main__":
    # Test and debug.
    pass
