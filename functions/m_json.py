import os
import json
import shutil
from typing import List, Any



def get_metadata_from_setup(path: str) -> dict:
    """Extracts IDs, names, and types from a setup JSON file.

    This function reads a JSON file with a specific structure and extracts IDs,
    names, and types. The returned dictionary contains an 'all' key with lists
    of all IDs and names, and keys for each unique type with lists of IDs and names
    for each type. Refer to README.md section "Runtime metadata" for a detailed description
    of the output data structure.

    Args:
        path (str): The file path to the setup JSON.

    Returns:
        dict: A dictionary with the metadata (IDs)

    """
    # TODO: Complete the function.
    
    #AUFRUF: get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')
    
    # Lese die JSON-Datei
    with open(path, 'r') as json_file:
        data = json.load(json_file)

    # Erstelle das Metadaten-Dictionary
    metadata_dict = {}

    # Extrahiere Werte und Namen aus der JSON-Datei und füge sie dem Metadaten-Dictionary hinzu
    for uuid, value in data.get('setup', {}).items():
        if 'type' in value and 'name' in value:
            type_name = value['type']
            name = value['name']
            #falls der Typ im dict noch nicht vorhanden ist, füge in hinzu
            if type_name not in metadata_dict:
                metadata_dict[type_name] = {'names': [], 'values': []}
            #füge Werte der Namen und uuids hinzu
            metadata_dict[type_name]['names'].append(name)
            metadata_dict[type_name]['values'].append(uuid)

    return metadata_dict

    # DONE #


def add_temperature_sensor_serials(folder_path: str, metadata: dict) -> None:
    """Adds temperature sensor serials to the provided metadata(IDs) dictionary.

    This function updates the given metadata dictionary by adding a list of sensor
    serial numbers extracted from datasheets located in the specified folder path.
    Refer to README.md section "Runtime metadata" for a detailed description
    of the location the serials have to be added. The function assumes a specific
    structure for the metadata dictionary and the datasheets.

    Args:
        folder_path (str): The path to the folder containing datasheets.
        metadata (dict): The metadata dictionary to be updated. The dictionary is expected
                         to have a 'sensor' key with a nested 'values' key containing UUIDs.

    """
    # TODO: Complete the function.
    #AUFRUFEN: add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets', get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json'))
    #Ausgeben der uuids der Sensoren
    sensor_uuids = metadata['sensor']['values']
    #leere Liste für serials erstellen
    serials = []
    #Schleife mit i als uuids der Sensoren durchgehen
    for i in sensor_uuids:
        #In die Serial Liste Serials aus den json Dateien der Sensoren mithilfe der get_json_entry Datei ablegen
        serials.append(get_json_entry(folder_path,i,['sensor','serial']))
    #serials in metadata hinzufügen
    metadata['sensor']['serials'] = serials
    
    return(metadata)
  
    # DONE #
    
def get_json_entry(folder_path: str, uuid: str, json_path: List[str]) -> Any:
    """Recursively searches for a specific JSON entry in a directory structure based on the given UUID and JSON path.

    This function searches the provided folder for JSON files and checks each file for a matching ID. If found,
    it navigates the JSON content using the provided JSON path and retrieves the corresponding entry. If the file
    or the desired path does not exist, it continues the search in other JSON files and subdirectories. The function
    expects the JSON files to have a structure where they contain a 'JSON' key with a nested 'ID' key.
    If a JSON file does not have this structure, a warning is printed, and the search continues.

    Args:
        folder_path (str): The path to the folder containing JSON files and subfolders.
        id (str): The UUID to be searched for within the JSON files.
        json_path (List[str]): A list of keys representing the path to the desired entry within a JSON file.

    Returns:
        Any: The desired JSON entry based on the provided UUID and JSON path.
             If the entry is not found, it returns None.

    Example:
        Given a folder structure:
        ├── folder_path/
        │   ├── file1.json
        │   └── subfolder/
        │       └── file2.json

        And the content of 'file1.json' being:
        {
            "JSON": {
                "ID": "some_id",
                "sensor": {
                    "serial": "12345"
                }
            }
        }

        >>> json_entry = get_json_entry('folder_path', 'some_id', ['JSON', 'sensor', 'serial'])
        >>> print(json_entry)
        "12345"

    """
    # Get the list of items (files and subfolders) in the given folder_path.
    folder_content = os.listdir(folder_path)
    # Initialize lists to store paths of JSON files and subfolders.
    json_files = []
    subfolders = []

    # Iterate over each item in the folder_content.
    for i in folder_content:
        # Construct the full path of the item.
        path = "{}/{}".format(folder_path, i)
        # Check if the item is a file.
        if os.path.isfile(path):
            # Check if the file has a ".json" extension.
            if i.split(".")[-1] == "json":
                # Add the path of the JSON file to the json_files list.
                json_files.append(path)
        # If the item is not a file, it's a subfolder.
        else:
            # Add the path of the subfolder to the subfolders list.
            subfolders.append(path)

    # Recursively check each subfolder for the JSON entry.
    for folder in subfolders:
        # Recursively call get_json_entry on the subfolder.
        json_entry = get_json_entry(folder, uuid, json_path)
        # If an entry is found in a subfolder, return it.
        if json_entry is not None:
            return json_entry

    # Check each JSON file in the current folder for the desired entry.
    for json_file in json_files:
        # Open and load the current JSON file.
        with open(json_file) as file:
            json_dict = json.load(file)

        # Try to retrieve the UUID from the JSON content.
        try:
            json_uuid = json_dict["JSON"]["ID"]
        # If the UUID is not present, print a warning and skip this file.
        except KeyError:
            print("Invalid json file.")
            continue

        # If the UUID from the JSON file matches the given uuid.
        if json_uuid == uuid:
            # Start with the entire JSON content.
            json_entry = json_dict
            # Navigate through the JSON content using the keys in json_path.
            for jp in json_path:
                # Drill down one level at a time.
                json_entry = json_entry[jp]
            # Return the desired JSON entry.
            return json_entry
    # If the function hasn't returned by this point, no matching entry was found, so return None.
    return None



def archiv_json(folder_path: str, setup_path: str, archiv_path: str) -> None:
    """Archives matching datasheets from a given folder based on setup datasheet.

    This function walks through the directory structure starting from the `folder_path`, looking for
    JSON files that match specific UUIDs defined in a setup file (`setup_path`). Any matching files are
    copied to the `archiv_path` directory, and their filenames are modified to include their UUIDs.
    The function expects the JSON files to have a structure where they contain a 'JSON' key with a nested
    'ID' key. If a JSON file does not have this structure, a warning is printed, and the file is skipped.

    Args:
        folder_path (str): The path to the root folder containing datasheets and subfolders to search.
        setup_path (str): The path to the setup datasheet file that contains the UUIDs to match against.
        archiv_path (str): The path to the folder where matching JSON files should be archived.
                           If the directory does not exist, it will be created.

    Example:
        Given a folder structure:
        ├── folder_path/
        │   ├── file1.json
        │   └── subfolder/
        │       └── file2.json

        And the content of 'file1.json' being:
        {
            "JSON": {
                "ID": "uuid1",
                ...
            }
        }

        And the content of the setup file being:
        {
            "setup": {
                "uuid1": {...},
                "uuid2": {...}
            }
        }

        After calling `archiv_json('folder_path', 'setup_path', 'archiv_path')`:
        >>> archiv_json('folder_path', 'setup_path', 'archiv_path')

        the `archiv_path` folder will contain:
        file1_uuid1.json

    """
    # Open and load the setup JSON file.
    with open(setup_path, "r") as f:
        setup_data = json.load(f)

    # Initialize lists to store paths of matching JSON files and their new names.
    matching_files = []
    copy_names = []

    # Walk through the directory structure starting from folder_path.
    for root, _, files in os.walk(folder_path):
        # Iterate over all files in the current directory.
        for file in files:
            # Check if the file is a JSON file.
            if file.endswith(".json"):
                # Construct the full path of the file.
                file_path = os.path.join(root, file)

                # Open and load the current JSON file.
                with open(file_path, "r") as f:
                    json_dict = json.load(f)

                # Try to get the UUID from the JSON file.
                try:
                    json_uuid = json_dict["JSON"]["ID"]
                # If the UUID is not present, print a warning and skip this file.
                except KeyError:
                    print("Invalid json file.")
                    continue

                # Iterate over UUIDs in the setup data.
                for uuid in tuple(setup_data["setup"].keys()):
                    # If the UUID from the JSON file matches a UUID in the setup data.
                    if uuid == json_uuid:
                        # Append the file path to the matching_files list.
                        matching_files.append(file_path)
                        # Construct the new name for the file and append it to the copy_names list.
                        copy_name = "{}_{}.json".format(file[:-5], uuid)
                        copy_names.append(copy_name)

    # Copy Setup
    matching_files.append(setup_path)
    copy_names.append(setup_path.split("/")[-1])

    # Check if the archive directory exists; if not, create it.
    if not os.path.exists(archiv_path):
        os.makedirs(archiv_path)
    # Iterate over the paths and names of the matching files.
    for path, name in zip(matching_files, copy_names):
        # Copy each matching file to the archive directory with its new name.
        shutil.copyfile(path, os.path.join(archiv_path, name))


if __name__ == "__main__":
    # Debug and Test
    pass
